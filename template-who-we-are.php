<?php
  /*
  Template Name: Who We Are
  */

  get_header();
?>

  <section class="about">

    <header class="about__header">
      <div class="g__container">
        <h1 class="about__header-title"><?php the_title(); ?></h1>
        <p class="about__header-subtitle"><?php the_field('subtitulo') ?></p>
      </div>
    </header>

    <div class="about__content">
      <div class="g__container">
        <div class="g__row">

          <div class="g__col-d-4 g__offset-d-1 g__col-t-12 g__col-m-12">
            <div class="about__content-text">
              <h2 class="about__content-text-title">QUEM FAZ A CLEVER LOOK</h2>
              <?php the_field('texto') ?>
            </div>
          </div>

          <div class="g__col-d-6 g__offset-d-1 g__col-t-12 g__col-m-12">
            <div class="about__content-team">
              <div class="about__content-team-person" style="background-image: url(<?php the_field('foto_andrea') ?>)">
                <div class="about__content-team-person__content">
                  <span class="about__content-team-person__content-name"><?php the_field('nome_andrea') ?></span>
                  <span class="about__content-team-person__content-role"><?php the_field('cargo_andrea') ?></span>
                </div>
              </div>

              <div class="about__content-team-person" style="background-image: url(<?php the_field('foto_nancy') ?>)">
                <div class="about__content-team-person__content">
                  <span class="about__content-team-person__content-name"><?php the_field('nome_nancy') ?></span>
                  <span class="about__content-team-person__content-role"><?php the_field('cargo_nancy') ?></span>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>

    <div class="instagram" >
      <div class="g__container">
        <a href="https://www.instagram.com/cleverlookbr/" class="instagram__link" title="Instagram" target="_blank">SIGA @CLEVERLOOKBR <br>NO INSTAGRAM</a>
        <ul class="instagram__list" id="instafeed"></ul>
      </div>
    </div>

  </section>

<?php get_footer(); ?>
