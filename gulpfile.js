/* ------------------------
  GULP TASKS
-------------------------*/

// REQUIRES
var gulp = require('gulp')
var gulpUtil = require('gulp-util')
var uglify = require('gulp-uglify')
var concat = require('gulp-concat')
var rename = require('gulp-rename')
var prefix = require('gulp-autoprefixer')
var imagemin = require('gulp-imagemin')
var browserSync = require('browser-sync').create()
var sass = require('gulp-sass')

// SCRIPTS TASK
gulp.task('concat-js', function() {
  gulp.src(['assets/src/js/base/*.js', 'assets/src/js/vendor/*.js', 'assets/src/js/scripts/*.js'])
  .pipe(concat('main.js'))
  .pipe(rename('main.min.js'))
  .pipe(uglify().on('error', gulpUtil.log))
  .pipe(gulp.dest('assets/js'))
});

// Image Minification
gulp.task('minify-img', function() {
  gulp.src('assets/src/img/*')
          .pipe(imagemin())
          .pipe(gulp.dest('assets/img'))
});


// Get the final SCSS file and convert to CSS
gulp.task('process-css', function() {
  gulp.src('assets/src/sass/global.scss')
  .pipe(sass({
    outputStyle: 'compressed'
  }).on('error', sass.logError))
  .pipe(prefix({
    browsers: ['last 2 versions']
  }))
  .pipe(gulp.dest('assets/css'))
  .pipe(browserSync.stream())
});


gulp.task('serve', ['concat-js', 'process-css'], function() {

  browserSync.init({
    proxy: "http://localhost:8888/brasj/"
  });

  gulp.watch("assets/src/js/**/*.js", ['concat-js']);
  gulp.watch('assets/src/sass/**/*.scss', ['process-css']);
  gulp.watch("./*.php").on('change', browserSync.reload);
  gulp.watch("./**/*.php").on('change', browserSync.reload);
});

// DEFAULT
gulp.task('default', ['serve']);
