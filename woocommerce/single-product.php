<?php get_header(); ?>

  <section class="product">
    <div class="g__container">

      <div class="product__images">
        <div class="product__images-swiper swiper-container">
          <div class="swiper-wrapper">

            <div class="swiper-slide">
              <div class="product__images-item" style="background-image: url(<?php bloginfo('template_url'); ?>/assets/img/thumb.png)"></div>
            </div>

            <div class="swiper-slide">
              <div class="product__images-item" style="background-image: url(<?php bloginfo('template_url'); ?>/assets/img/thumb.png)"></div>
            </div>

          </div>

          <div class="swiper-button-prev swiper-button-white"></div>
          <div class="swiper-button-next swiper-button-white"></div>
        </div>

        <div class="product__images-thumbs">
          <div class="product__images-thumbs-item" style="background-image: url(<?php bloginfo('template_url'); ?>/assets/img/thumb.png)"></div>

          <div class="product__images-thumbs-item" style="background-image: url(<?php bloginfo('template_url'); ?>/assets/img/thumb.png)"></div>
        </div>

        <a href="<?php bloginfo( 'url' ); ?>/carrinho/?add-to-cart=<?php echo get_the_ID(); ?>" class="how-works__header-link buy-button">Adquirir</a>
      </div>

      <?php if (have_posts()) : ?>
      <?php while (have_posts()) : the_post(); ?>
      <div class="product__content">
        <p class="product__cat">
          <?php
          $terms =  get_the_terms( $post->ID, 'product_cat' );
            if ( $terms && ! is_wp_error( $terms ) ) {
                echo $terms[0]->name;
            }
          ?>
        </p>
        <h1 class="product__title"><?php the_title(); ?></h1>
        <span class="product__sku">cod. <?php echo $product->get_sku(); ?></span>

        <p class="product__price"><?php echo $product->get_price_html(); ?></p>

        <div class="product__description">
          <h2 class="product__description-title">Descrição</h2>

            <?php the_content(); ?>

        </div>

        <?php
        global $product;
        $sizes = $product->get_attribute( 'tamanhos' ); $sizes = explode(", ",$sizes); ?>

        <?php if($sizes[0]!=''){ // if product sizes are defined ?>
           <div class="product__sizes">
             <p class="product__sizes-label">Selecione um Tamanho</p>

             <ul class="product__sizes-list">
              <?php foreach($sizes as $size){ ?>
                <li class="product__sizes-item"><?php echo $size; ?></li>
              <?php } ?>
              </ul>
          </div>
        <?php } ?>

        <a href="<?php bloginfo( 'url' ); ?>/carrinho/?add-to-cart=<?php echo get_the_ID(); ?>" class="product__buy buy-button">ADQUIRIR</a>
      </div>
      <?php endwhile; ?>
        <?php else : ?>
      <?php endif; ?>


      </div>
    </div>
  </section>

  <?php get_template_part('template-parts/newsletter'); ?>
<?php get_footer(); ?>
