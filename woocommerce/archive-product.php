<?php get_header(); ?>
  <section class="shop">

    <div class="g__container">
      <header class="faq__head head">
        <h1 class="head__title">Produtos</h1>
        <p class="head__subtitle"><?php the_field('subtitulo', '2472') ?></p>
      </header>
    </div>

    <div class="shop__content">
      <div class="g__container">
        <div class="g__row">

          <div class="g__col-d-3 g__col-t-12 g__col-m-12">
            <ul class="shop__cats">
            <?php
              $taxonomy     = 'product_cat';
              $orderby      = 'name';
              $show_count   = 0;      // 1 for yes, 0 for no
              $pad_counts   = 0;      // 1 for yes, 0 for no
              $hierarchical = 1;      // 1 for yes, 0 for no
              $title        = '';
              $empty        = 0;

              $args = array(
                     'taxonomy'     => $taxonomy,
                     'orderby'      => $orderby,
                     'show_count'   => $show_count,
                     'pad_counts'   => $pad_counts,
                     'hierarchical' => $hierarchical,
                     'title_li'     => $title,
                     'hide_empty'   => $empty
              );
             $all_categories = get_categories( $args );
             foreach ($all_categories as $cat) {
                if($cat->category_parent == 0) {
                    $category_id = $cat->term_id;
                    echo '<li><a class="shop__cats-main" href="'. get_term_link($cat->slug, 'product_cat') .'">'. $cat->name .'</a></li>';

                    $args2 = array(
                            'taxonomy'     => $taxonomy,
                            'child_of'     => 0,
                            'parent'       => $category_id,
                            'orderby'      => $orderby,
                            'show_count'   => $show_count,
                            'pad_counts'   => $pad_counts,
                            'hierarchical' => $hierarchical,
                            'title_li'     => $title,
                            'hide_empty'   => $empty
                    );
                    $sub_cats = get_categories( $args2 );
                    if($sub_cats) {
                        foreach($sub_cats as $sub_category) {
                            echo '<li><a class="shop__cats-sub" href="'. get_term_link($sub_category->slug, 'product_cat') .'">'. $sub_category->name .'</a></li>';
                        }
                    }
                }
            }
            ?>
            </ul>
          </div>

          <div class="g__col-d-9 g__col-t-12 g__col-m-12">
            <ul class="shop__list">
              <p class="shop__list-title">Últimos Lançamentos</p>

              <div class="g__row">
                <?php
                  $custom_query_args = array(
                    'post_type' => 'product',
                    'posts_per_page' => 9,
                  );

                  // Get current page and append to custom query parameters array
                  $custom_query_args['paged'] = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;

                  // Instantiate custom query
                  $custom_query = new WP_Query( $custom_query_args );

                  // Pagination fix
                  $temp_query = $wp_query;
                  $wp_query   = NULL;
                  $wp_query   = $custom_query;

                  // Output custom query loop
                  if ( $custom_query->have_posts() ) :
                      while ( $custom_query->have_posts() ) :
                          $custom_query->the_post();
                        ?>
                        <div class="g__col-d-4 g__col-t-6 g__col-m-12">
                           <li class="shop__list-item">
                             <a href="<?php the_permalink(); ?>" class="shop__list-item-image" style="background-image: url(<?php the_post_thumbnail_url(); ?>)">
                               <span class="shop__list-item-more">+</span>
                             </a>

                             <a href="<?php the_permalink(); ?>" class="shop__list-item-content">
                               <h2 class="shop__list-item-content-title"><?php the_title(); ?></h2>
                               <span class="shop__list-item-content-price"><?php echo $product->get_price_html(); ?></span>
                             </a>
                           </li>
                         </div>
                        <?php
                      endwhile;
                  endif;
                  // Reset postdata
                  wp_reset_postdata();

                  // Custom query loop pagination

                  // Reset main query object
                  $wp_query = NULL;
                  $wp_query = $temp_query;
                ?>
              </div>
            </ul>

              <?php pagination(); ?>

          </div>


        </div>
      </div>
    </div>

    <?php get_template_part( 'template-parts/newsletter');?>

  </section>
<?php get_footer(); ?>
