<?php
  /*
  Template Name: Homepage
  */

  get_header();
?>

<section class="main-slider">
  <div class="g__container">
    <div class="main-slider__container swiper-container">
      <div class="swiper-wrapper">

        <?php if(have_rows('slides')): while(have_rows('slides')) : the_row(); ?>
          <div class="swiper-slide">
            <div class="main-slider__item">

              <div class="main-slider__item-text">
                <div class="main-slider__item-text-content">
                  <h2 class="main-slider__item-text-title"><?php the_sub_field('titulo_do_slide') ?></h2>
                  <p class="main-slider__item-text-subtitle"><?php the_sub_field('subtitulo_do_slide') ?></p>
                  <?php if(get_sub_field('link_do_slide')): ?>
                    <a href="<?php the_sub_field('link_do_slide') ?>" class="link">
                      <span class="link__label">Veja mais</span>
                    </a>
                  <?php endif; ?>
                </div>
              </div>

              <div class="main-slider__item-image" style="background-image: url(<?php the_sub_field('imagem_do_slide') ?>)"></div>

            </div>
          </div>
        <?php endwhile; else : endif; ?>

      </div>

      <div class="swiper-button-prev swiper-button-white"></div>
      <div class="swiper-button-next swiper-button-white"></div>
    </div>
  </div>
</section>

<?php get_template_part( 'template-parts/newsletter');?>

<section class="featured-content">
  <div class="g__container">

    <div class="g__row g__row--alignHorizontal">
      <div class="featured-content__text">
        <h3 class="featured-content__text-title"><?php the_field('titulo_destaque') ?></h3>

        <div class="featured-content__text-content"><?php the_field('texto_destaque') ?></div>

        <?php if(get_field('link_destaque')) :  ?>
          <a href="<?php the_field('link_destaque') ?>" class="link">
            <span class="link__label">Conheça</span>
          </a>
        <?php endif; ?>
      </div>

      <div class="featured-content__image" style="background-image: url(<?php the_field('imagem_destaque') ?>)"></div>
    </div>
  </div>
</section>

<section class="instagram">
  <div class="g__container">
    <a href="https://www.instagram.com/cleverlookbr/" class="instagram__link" title="Instagram" target="_blank">SIGA @CLEVERLOOKBR <br>NO INSTAGRAM</a>
    <ul class="instagram__list" id="instafeed"></ul>
  </div>
</section>

<section class="testimonies">
  <div class="testimonies__container g__container">
    <h3 class="testimonies__title">Depoimentos</h3>

    <div class="testimonies__swiper swiper-container">
      <div class="swiper-wrapper">

        <?php if(have_rows('depoimentos')): while(have_rows('depoimentos')) : the_row(); ?>
          <div class="swiper-slide">
            <div class="testimonies__item">
              <p class="testimonies__item-text">“<?php the_sub_field('texto_do_depoimento'); ?>“</p>

              <p class="testimonies__item-info">
                <span class="testimonies__item-info-author"><?php the_sub_field('autor_do_depoimento'); ?></span>
                <span class="testimonies__item-info-role"><?php the_sub_field('descricao_do_autor'); ?></span>
              </p>
            </div>
          </div>
        <?php endwhile; else : endif; ?>

      </div>
    </div>

  </div>
</section>

<section class="last-news">
  <div class="g__container">
    <h2 class="last-news__title">Últimas Notícias</h2>

    <div class="g__row">

      <?php
				$posts = get_field('ultimas_noticias__');
				$args = array(
					'post_type'   => 'post',
					'posts_per_page'         => 3,
					'post__in'     => $posts,

				);
				$produto = new WP_Query( $args );

				while($produto->have_posts()) { $produto->the_post();
      ?>

      <div class="g__col-d-4 g__col-m-12">
        <a href="<?php the_permalink(); ?>" class="post-thumb" style="background-image: url(<?php the_post_thumbnail_url(); ?>)">
          <h2 class="post-thumb__title"><?php the_title(); ?></h2>
        </a>
      </div>

      <?php } wp_reset_postdata(); wp_reset_query();?>

    </div>
  </div>
</section>

<section class="last-looks">
  <div class="last-looks__container g__container">
    <h2 class="last-looks__title">Últimos Looks</h2>

    <div class="g__row">
      <?php if(get_field('ultimos_looks__')) : ?>
        <?php
          $looks = get_field('ultimos_looks__');
          $args = array(
            'post_type'   => 'product',
            'posts_per_page'         => 3,
            'post__in'     => $looks,

          );
          $looksList = new WP_Query( $args );

          while($looksList->have_posts()) { $looksList->the_post();
        ?>

        <div class="g__col-d-4 g__col-t-12 g__col-m-12">
          <div class="last-looks__item">
            <div class="last-looks__item-image" style="background-image: url(<?php the_post_thumbnail_url(); ?>)"></div>

            <div class="last-looks__item-text">
              <h3 class="last-looks__item-title"><?php the_title(); ?></h3>
              <p class="last-looks__item-subtitle"><?php the_field('subtitulo_do_produto'); ?></p>
              <p class="last-looks__item-price"><?php echo $product->get_price_html(); ?></p>
              <a href="<?php the_permalink(); ?>" class="last-looks__item-link">Ver Look</a>
            </div>
          </div>
        </div>

        <?php } wp_reset_postdata(); wp_reset_query();?>
      <?php endif; ?>

    </div>
  </div>
</section>

<?php get_footer(); ?>
