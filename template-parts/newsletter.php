<div class="newsletter">
  <div class="newsletter__container g__container">
    <h3 class="newsletter__title"><?php the_field('titulo_newsletter', '103') ?></h3>
    <p class="newsletter__subtitle"><?php the_field('subtitulo_newsletter', '103') ?></p>
    <?php echo do_shortcode('[contact-form-7 id="2495" title="Newsletter Clever"]') ?>
  </div>
</div>
