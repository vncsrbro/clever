<?php
  /*
  Template Name: Contact
  */

  get_header();
?>

  <section class="contact">
    <div class="g__container">
      <div class="g__row g__row--columnsNoMarginBottom">

        <div class="g__col-d-6 g__col-t-12 g__col-m-12 g__col--noMargin">
          <div class="contact__text contact--sameHeight">
            <div class="contact__text-content">
              <h1 class="contact__text-title"><?php the_title(); ?></h1>
              <p class="contact__text-subtitle"><?php the_field('subtitulo'); ?></p>

              <p class="contact__text-line contact__text-line--address"><?php the_field('endereco'); ?></p>
              <p class="contact__text-line contact__text-line--phone"><?php the_field('telefone'); ?></p>
              <p class="contact__text-line contact__text-line--mail"><?php the_field('email'); ?></p>
            </div>
          </div>
        </div>

        <div class="g__col-d-6 g__col-t-12 g__col-m-12 g__col--noMargin">
          <div class="contact__form contact--sameHeight">
            <div class="contact__form-content">
              <p class="contact__form-title">Escreve um e-mail pra gente</p>
              <?php echo do_shortcode('[contact-form-7 id="2469" title="Contato Clever"]') ?>
            </div>
          </div>
        </div>

      </div>
    </div>
  </section>

<?php get_footer(); ?>
