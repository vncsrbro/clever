<!DOCTYPE html>
<html lang="pt_BR" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title><?php wp_title( '|', true, 'right' ); ?> <?php bloginfo('name'); ?></title>
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <?php wp_head(); ?>
  </head>
  <body>

    <main class="site-wrapper">

      <header class="header">
        <div class="header__container">
          <div class="header__logo">
            <a href="<?php bloginfo('url'); ?>">
              <img src="<?php bloginfo('template_url'); ?>/assets/img/logo__clever.svg" alt="Clever Look" class="header__logo-image">
            </a>
          </div>

          <nav class="header__menu">

            <div class="header__menu-toggle">
              <span class="header__menu-toggle-line"></span>
              <span class="header__menu-toggle-line"></span>
              <span class="header__menu-toggle-line"></span>
            </div>

            <ul class="header__menu-list">

              <?php if(have_rows('menu_topo', '103')): while(have_rows('menu_topo', '103')) : the_row(); ?>
                <li class="header__menu-item">
                  <a href="<?php the_sub_field('url') ?>" class="header__menu-link"><?php the_sub_field('label') ?></a>
                </li>
              <?php endwhile; else : endif; ?>

            </ul>
          </nav>

        </div>
      </header>
