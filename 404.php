<?php get_header(); ?>
  <section class="not-found">
    <div class="g__container">
      <span class="not-found__detail">404</span>
      <h1 class="not-found__title">Página Não Encontrada</h1>
    </div>
  </section>
<?php get_footer(); ?>
