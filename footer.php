      <footer class="footer">
        <div class="g__container">
          <div class="g__row g__row--columnsNoMarginBottom g__row--alignHorizontal">

            <div class="g__col-d-4 g__col-m-12">
              <ul class="footer__links">

                <?php if(have_rows('menu_rodape_esquerda', '103')): while(have_rows('menu_rodape_esquerda', '103')) : the_row(); ?>
                  <li>
                    <a href="<?php the_sub_field('url') ?>" class="footer__links-item"><?php the_sub_field('label') ?></a>
                  </li>
                <?php endwhile; else : endif; ?>

              </ul>
            </div>

            <div class="g__col-d-4 g__col-m-12">
              <div class="footer__logo">
                <img src="<?php bloginfo('template_url'); ?>/assets/img/icon__brand.svg" alt="Clever Look">
              </div>
            </div>

            <div class="g__col-d-4 g__col-m-12">
              <ul class="footer__links footer__links--right">
                <?php if(have_rows('menu_rodape_direita', '103')): while(have_rows('menu_rodape_direita', '103')) : the_row(); ?>
                  <li>
                    <a href="<?php the_sub_field('url') ?>" class="footer__links-item"><?php the_sub_field('label') ?></a>
                  </li>
                <?php endwhile; else : endif; ?>
              </ul>
            </div>

          </div>

        </div>
      </footer>

      <div class="copyright">
        <div class="g__container">
          <div class="copyright__text">
            Todos os direitos reservados - Clever Look <?php echo date('Y'); ?>
          </div>

          <div class="copyright__credits">
            Desenvolvido por <a href="https://ribeirovinicius.com" class="copyright__credits-link" title="Vinicius Ribeiro" target="_blank">Vinicius Ribeiro</a>
          </div>
        </div>
      </div>

    </main>

    <?php wp_footer(); ?>
  </body>
</html>
