<?php
  /*
  Template Name: How Works
  */

  get_header();
?>

<section class="how-works">

  <header class="how-works__header">
    <div class="g__container">
      <div class="g__row">

        <div class="g__col-d-6 g__col-t-6 g__col-m-12 g__col--noMargin">
          <div class="how-works__header-content">
            <div class="how-works__header-content-box">
              <h1 class="how-works__header-title"><?php the_title(); ?></h1>
              <p class="how-works__header-subtitle"><?php the_field('subtitulo'); ?></p>

              <a href="#como-funciona" class="scroll how-works__header-link">Saiba Mais</a>
            </div>
          </div>
        </div>

        <div class="g__col-d-6 g__col-t-6 g__col--noMargin m-hide">
          <div class="how-works__header-image" style="background-image: url(<?php the_field('imagem_principal') ?>)"></div>
        </div>

      </div>
    </div>
  </header>

  <div class="featured-content" id="como-funciona">
    <div class="g__container">

      <div class="g__row g__row--alignHorizontal">
        <div class="featured-content__text">
          <h2 class="featured-content__text-title"><?php the_field('titulo_secundario') ?></h2>

          <div class="featured-content__text-content"><?php the_field('texto') ?></div>

          <ul class="featured-content__list">
            <?php if(have_rows('topicos')): while(have_rows('topicos')) : the_row(); ?>
              <li class="featured-content__list-item"><?php the_sub_field('topico') ?></li>
            <?php endwhile; else : endif; ?>
          </ul>
        </div>

        <div class="featured-content__image" style="background-image: url(<?php bloginfo('template_url'); ?>/assets/img/thumb.png)"></div>
      </div>
    </div>
  </div>

  <div class="how-works__list">
    <div class="g__container">
      <div class="g__row">

        <div class="g__col-d-12">
          <h2 class="how-works__list-title">PEQUENAS COISAS QUE <br>FAZEM A DIFERENÇA</h2>
        </div>

        <?php if(have_rows('etapas')): while(have_rows('etapas')) : the_row(); ?>

        <div class="g__col-d-3 g__col-t-6 g__col-m-12">
          <div class="how-works__list-item">
            <div class="how-works__list-item-image" style="background-image: url(<?php the_sub_field('imagem_ilustrativa') ?>)">
              <h3 class="how-works__list-item-title"><?php the_sub_field('titulo_da_etapa') ?></h3>
            </div>

            <div class="how-works__list-item-overlay">
              <div class="how-works__list-item-overlay-close">
                <span class="how-works__list-item-overlay-close-plus">+</span>
              </div>

              <div class="how-works__list-item-overlay-box">
                <span class="how-works__list-item-overlay-title"><?php the_sub_field('titulo_da_etapa') ?></span>
                <p class="how-works__list-item-overlay-subtitle"><?php the_sub_field('descricao_da_etapa') ?></p>
              </div>
            </div>
          </div>
        </div>
        <?php endwhile; else : endif; ?>

      </div>
    </div>
  </div>
</section>

<?php get_footer(); ?>
