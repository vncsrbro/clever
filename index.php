<?php get_header(); ?>

  <section class="blog">
    <header class="about__header">
      <div class="g__container">
        <h1 class="about__header-title">Blog</h1>
        <p class="about__header-subtitle"><?php the_field('subtitulo', '105') ?></p>
      </div>
    </header>

    <div class="featured-post">
      <div class="g__container">

        <div class="featured-post__content">
          <div class="featured-post__content-box">
            <h2 class="featured-post__content-title"><?php the_field('titulo_destaque', '105') ?></h2>
            <p class="featured-post__content-subtitle"><?php the_field('descricao_destaque', '105') ?></p>
            <a href="<?php the_field('link_destaque', '105') ?>" class="link">
              <span class="link__label">Veja mais</span>
            </a>

          </div>
        </div>

        <div class="featured-post__image" style="background-image: url(<?php the_field('imagem_destaque', '105') ?>)"></div>
      </div>
    </div>

    <div class="blog__list">
      <div class="g__container">
        <div class="g__row">

          <div class="g__col-d-12">
            <h2 class="blog__list-title"><?php the_field('titulo_first', '105') ?></h2>
            <p class="blog__list-subtitle"><?php the_field('subtitulo_first', '105') ?></p>
          </div>

          <?php if(get_field('posts_relacionados_first', '105')) : ?>
            <?php
  						$posts = get_field('posts_relacionados_first', '105');
  						$args = array(
  							'post_type'   => 'post',
  							'posts_per_page'         => 3,
  							'post__in'     => $posts,

  						);
  						$produto = new WP_Query( $args );

  						while($produto->have_posts()) { $produto->the_post(); ?>
                <div class="g__col-d-4 g__col-m-12">
                  <a href="<?php the_permalink(); ?>" class="post-thumb" style="background-image: url(<?php the_post_thumbnail_url(); ?>)">
                    <h3 class="post-thumb__title"><?php the_title(); ?></h3>
                  </a>
                </div>
						<?php } wp_reset_postdata(); wp_reset_query();?>
          <?php endif; ?>

        </div>
      </div>
    </div>

    <div class="blog__list">
      <div class="g__container">
        <div class="g__row">

          <div class="g__col-d-12">
            <h2 class="blog__list-title"><?php the_field('titulo_second', '105') ?></h2>
            <a href="<?php the_field('link_da_categoria_second', '105') ?>" class="blog__list-link">VER MAIS</a>
            <p class="blog__list-subtitle"><?php the_field('subtitulo_second', '105') ?></p>
          </div>

          <?php if(get_field('posts_relacionados_second', '105')) : ?>
            <?php
  						$posts = get_field('posts_relacionados_second', '105');
  						$args = array(
  							'post_type'   => 'post',
  							'posts_per_page'         => 3,
  							'post__in'     => $posts,

  						);
  						$produto = new WP_Query( $args );

  						while($produto->have_posts()) { $produto->the_post(); ?>
                <div class="g__col-d-4 g__col-m-12">
                  <a href="<?php the_permalink(); ?>" class="post-thumb" style="background-image: url(<?php the_post_thumbnail_url(); ?>)">
                    <h3 class="post-thumb__title"><?php the_title(); ?></h3>
                  </a>
                </div>
						<?php } wp_reset_postdata(); wp_reset_query();?>
          <?php endif; ?>

        </div>
      </div>
    </div>

    <div class="blog__list">
      <div class="g__container">
        <div class="g__row">

          <div class="g__col-d-12">
            <h2 class="blog__list-title"><?php the_field('titulo_third', '105') ?></h2>
            <a href="<?php the_field('link_da_categoria_third', '105') ?>" class="blog__list-link">VER MAIS</a>
            <p class="blog__list-subtitle"><?php the_field('subtitulo_third', '105') ?></p>
          </div>

          <?php if(get_field('posts_relacionados_third', '105')) : ?>
            <?php
  						$posts = get_field('posts_relacionados_third', '105');
  						$args = array(
  							'post_type'   => 'post',
  							'posts_per_page'         => 3,
  							'post__in'     => $posts,

  						);
  						$produto = new WP_Query( $args );

  						while($produto->have_posts()) { $produto->the_post(); ?>
                <div class="g__col-d-4 g__col-m-12">
                  <a href="<?php the_permalink(); ?>" class="post-thumb" style="background-image: url(<?php the_post_thumbnail_url(); ?>)">
                    <h3 class="post-thumb__title"><?php the_title(); ?></h3>
                  </a>
                </div>
						<?php } wp_reset_postdata(); wp_reset_query();?>
          <?php endif; ?>

        </div>
      </div>
    </div>

  </section>
<?php get_footer(); ?>
