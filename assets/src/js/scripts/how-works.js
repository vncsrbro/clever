$('.how-works__list-item-image').click(function() {
  $(this).parent().toggleClass('how-works__list-item--active');
});

$('.how-works__list-item-overlay-close').click(function() {
  $(this).parent().parent().removeClass('how-works__list-item--active');
});

$('.scroll').click(function(e) {
  e.preventDefault();

  var id = $(this).attr('href');
  targetOffset = $(id).offset().top;

  $('html, body').animate({
    scrollTop: targetOffset - 85
  }, 500)
})
