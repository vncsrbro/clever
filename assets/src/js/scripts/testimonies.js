var testimoniesSlider = new Swiper('.testimonies__swiper', {
  slidesPerView: 2,
  spaceBetween: 60,
  loop: true,
  autoplay: {
    delay: 3000,
  },
  breakpoints: {
    959: {
      slidesPerView: 1
    }
  }
});
