var productSlider = new Swiper('.product__images-swiper', {
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
});

$('.product__images-thumbs-item:first-child').addClass('product__images-thumbs-item--active');

$('.product__images-thumbs-item').click(function() {
  $('.product__images-thumbs-item').removeClass('product__images-thumbs-item--active');
  $(this).addClass('product__images-thumbs-item--active');

  var thumbIndex = $(this).index();
  productSlider.slideTo(thumbIndex);
});

var sizesList = $('.product__sizes');
var sizesItem = $('.product__sizes-item');
var addToCardLink = $('.buy-button').attr('href');

sizesItem.click(function() {
  sizesList.addClass('product__sizes--clicked');

  sizesItem.removeClass('product__sizes-item--active');

  $(this).addClass('product__sizes-item--active');

  var sizesItemValue = $(this).html();

  var sizesAttributeParameter = '&attribute_pa_tamanhos=' + sizesItemValue;

  $('.buy-button').attr('href', ''+addToCardLink+sizesAttributeParameter+'');
  $('.buy-button').addClass('buy-button--active');
});
