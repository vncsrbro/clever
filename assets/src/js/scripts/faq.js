$('.faq__modal-content-close, .faq__modal-overlay').click(function() {
  $('.faq__modal').removeClass('faq__modal--active')
})

$('.faq__questions-item').click(function() {
  itemQuestion = $(this).attr('data-question');
  itemAnswer = $(this).attr('data-answer');

  var faqContent =
    '<span class="faq__modal-content-text-title">'+itemQuestion+'</span> ' +
    '<p class="faq__modal-content-text-answer">'+itemAnswer+'</p>';

  $('.faq__modal-content-text').html(faqContent);

  $('.faq__modal').addClass('faq__modal--active')
});
