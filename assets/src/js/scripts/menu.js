$('.header__menu-toggle').click(function() {
  var menuList = $('.header__menu-list');

  $(this).toggleClass('header__menu-toggle--active');

  menuList.slideToggle();
});
