<?php
  /*
  Template Name: FAQ
  */

  get_header();
?>

  <section class="faq">
    <div class="g__container">
      <header class="faq__head head">
        <h1 class="head__title"><?php the_title(); ?></h1>
        <p class="head__subtitle"><?php the_field('subtitulo') ?></p>
      </header>
    </div>

    <div class="faq__content">
      <div class="g__container">
        <div class="g__row">

          <div class="g__col-d-3 g__col-t-4 g__col-m-12">
            <aside class="faq__sidebar">
              <h2 class="faq__sidebar-title">Categorias</h2>

              <ul class="faq__sidebar-categories">
                <li class="faq__sidebar-categories-item" data-filter="all">Todas</li>
                <?php if(have_rows('categorias')): while(have_rows('categorias')) : the_row(); ?>
                <li class="faq__sidebar-categories-item" data-filter=".<?php the_sub_field('categoria') ?>"><?php the_sub_field('categoria') ?></li>
                <?php endwhile; else : endif; ?>
              </ul>

            </aside>
          </div>

          <div class="g__col-d-9 g__col-t-8 g__col-m-12">
            <div class="faq__questions">
              <div class="g__row faq__filters">

                <?php if(have_rows('perguntas')): while(have_rows('perguntas')) : the_row(); ?>
                  <div class="mix <?php the_sub_field('categoria') ?> faq__item g__col-d-4 g__col-t-6 g__col-m-12">
                    <div class="faq__questions-item faq__questions-item--<?php the_sub_field('cor') ?>" data-question="<?php the_sub_field('pergunta') ?>" data-answer="<?php the_sub_field('resposta') ?>">
                      <h3 class="faq__questions-item-title"><?php the_sub_field('pergunta') ?></h3>
                    </div>
                  </div>
                <?php endwhile; else : endif; ?>

              </div>
            </div>
          </div>

        </div>
      </div>
    </div>

    <div class="faq__modal">
      <div class="faq__modal-overlay"></div>

      <div class="faq__modal-content">
        <div class="faq__modal-content-close">+</div>
        <div class="faq__modal-content-text"></div>
      </div>
    </div>

    <?php get_template_part( 'template-parts/newsletter');?>
  </section>

<?php get_footer(); ?>

<script type="text/javascript">
  var containerEl = document.querySelector('.faq__filters');

  var mixer = mixitup(containerEl, {
      selectors: {
          target: '.faq__item'
      },
      animation: {
          duration: 300
      }
  });


  $('.faq__filters .mix').matchHeight();
</script>
