<?php get_header(); ?>

  <section class="post">


    <div class="featured-post">
      <div class="g__container">

        <div class="featured-post__content post__same-height">
          <div class="featured-post__content-box">
            <a href="<?php bloginfo('url'); ?>/blog/" class="featured-post__content-back">Voltar</a>
            <h2 class="featured-post__content-title"><?php the_title(); ?></h2>
            <p class="featured-post__content-subtitle"><?php the_field('subtitulo') ?></p>

          </div>
        </div>

        <div class="featured-post__image post__same-height" style="background-image: url(<?php the_post_thumbnail_url(); ?>)"></div>
      </div>
    </div>

    <div class="post__content">
      <div class="g__container">
        <div class="g__row">
          <div class="g__col-d-6 g__col-m-12 g__offset-d-3 g__offset-t-3">
            <?php if (have_posts()) : ?>
            <?php while (have_posts()) : the_post(); ?>
              <?php the_content(); ?>
            <?php endwhile; ?>
              <?php else : ?>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>


  </section>

<?php get_footer(); ?>
