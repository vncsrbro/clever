<?php get_header(); ?>

  <section class="blog">
    <header class="about__header">
      <div class="g__container">
        <h1 class="about__header-title"><?php the_archive_title(); ?></h1>
      </div>
    </header>

    <div class="blog__list blog__list--archive">
      <div class="g__container">
        <div class="g__row">

          <?php if (have_posts()) : ?>
          <?php while (have_posts()) : the_post(); ?>

            <div class="g__col-d-4 g__col-m-12">
              <a href="<?php the_permalink(); ?>" class="post-thumb" style="background-image: url(<?php the_post_thumbnail_url(); ?>)">
                <h3 class="post-thumb__title"><?php the_title(); ?></h3>
              </a>
            </div>

          <?php endwhile; ?>
            <?php else : ?>

          <?php endif; ?>

        </div>

        <?php pagination()?>
      </div>
    </div>

  </section>
<?php get_footer(); ?>
