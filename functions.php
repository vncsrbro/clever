<?php

  /**
   * Removing scripts and styles from head
  */
  remove_action('wp_head', 'rsd_link');
  remove_action('wp_head', 'wlwmanifest_link');
  remove_action('wp_head', 'start_post_rel_link', 10, 0 );
  remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
  remove_action('wp_head', 'feed_links_extra', 3);
  remove_action('wp_head', 'wp_generator');
  remove_action('wp_head', 'print_emoji_detection_script', 7);
  remove_action('admin_print_scripts', 'print_emoji_detection_script');
  remove_action('wp_print_styles', 'print_emoji_styles');
  remove_action('admin_print_styles', 'print_emoji_styles');

  /**
   * Enqueue scripts and styles.
  */
  function site_scripts() {
    wp_enqueue_style( 'site-style', get_template_directory_uri() . '/assets/css/global.css', array(), '20181203');
    wp_enqueue_script( 'site-main', get_template_directory_uri() . '/assets/js/main.min.js', array(), '20181203', true );
  }
  add_action( 'wp_enqueue_scripts', 'site_scripts' );

  /**
   * Theme Supports
  */
  add_theme_support('post-thumbnails');
  add_theme_support('woocommerce');

  /**
   * Change number of products that are displayed per page (shop page)
   */
  add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );

  function new_loop_shop_per_page( $cols ) {
    // $cols contains the current number of products per page based on the value stored on Options -> Reading
    // Return the number of products you wanna show per page.
    $cols = 9;
    return $cols;
  }

  /**
   * Disable menu bar
  */
  show_admin_bar(false);

  /**
   * Removing links from menu
  */
  // add_action( 'admin_menu', 'my_remove_menu_pages' );

  /**
   * Removing links from sidebar
  */
  function my_remove_menu_pages() {
    // remove_menu_page('edit.php');
    remove_menu_page('edit-comments.php');
  }

  /**
   * WP Title
  */
  // function setup_theme() {
  //  add_theme_support('title-tag');
  // }
  //
  // add_action( 'after_setup_theme', 'setup_theme' );

  /**
   * Pagination
  */
  //Paginação
  function pagination($pages = '', $range = 5)
  {
    $showitems = ($range * 2)+1;
    global $paged;
    if(empty($paged)) $paged = 1;
    if($pages == ''){
    global $wp_query;
    $pages = $wp_query->max_num_pages;
    if(!$pages){
    $pages = 1;
    }
  }
  if(1 != $pages){
  echo "<div class=\"pagination\"><span class=\"current\">Página ".$paged." de ".$pages."</span>";
    if($paged > 1 && $showitems < $pages) echo "<a title=\"Anterior\" href='".get_pagenum_link($paged - 1)."'>&laquo;</a>";
      for ($i=1; $i <= $pages; $i++)
    {
    if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
    {
      echo ($paged == $i)? "<span class=\"current-page\">".$i."</span>":"<a title=\"Página $i\" href='".get_pagenum_link($i)."' class=\"inactive\">".$i."</a>";
    }
    }
    if ($paged < $pages && $showitems < $pages) echo "<a title=\"Próxima\" href=\"".get_pagenum_link($paged + 1)."\">&raquo;</a>";
      echo "</div>\n";
    }
  }
  /**
   * Custom login
  */
  function my_custom_login_logo() {
    echo '
  <style type="text/css">
  body.login {
  	background: #efeeee !important
  }

  .login h1 a {
    background: url(./wp-content/themes/brasj/assets/img/logo-brasj.svg) no-repeat !important;
    background-size: cover;
    height: 60px !important;
    margin-bottom: 0 !important
  }

  .login h1 {
  	text-align: center;
  	background:#fff;
  	border:1px solid #fff;
  	border-top-left-radius: 20px;
  	border-top-right-radius: 20px;
  	height: auto;
  	z-index: 10;
  	outline:none;
  	position: relative;
  	padding-top: 20px;
  	box-sizing: border-box;

  }

  .login form {
      margin-top: -10px;
      margin-left: 0;
      z-index: 6;
      position: relative;
      padding: 26px 24px 46px;
      background: #fff;
      -webkit-box-shadow: 0 1px 3px rgba(0,0,0,.13);
      box-shadow: 0 1px 3px rgba(0,0,0,.13);
  }
  </style>';}
  add_action('login_head', 'my_custom_login_logo');

  /// setando principais configuracoes de CSS da página

  function new_custom_login_page_style() {
      echo '
      <style type="text/css">
      body.login {
  		background:white;
  		background-size:cover;
  		overflow:hidden;
  		font-family: Verdana, sans-serif;background-size:cover
  		}
      .login #nav a, .login #backtoblog a {color:#fff!important; text-decoration:none !important; font-size: 0.9em !important;}
      .login #nav a:hover, .login #backtoblog a:hover {color:#fff!important; text-decoration:underline !important;}
      .login #nav {float: left !important;}
      .login form {; border-radius: 10px !important; -webkit-border-radius: 10px !important;}
      </style>';}
  add_action('login_head', 'new_custom_login_page_style');

  // SHORTCODE to create box in Posts
  function noteBox_shortcode( $atts, $content = null ) {
    return '<div class="note-box">' . $content . '</div>';
  }
  add_shortcode( 'noteBox', 'notebox_shortcode' );

?>
